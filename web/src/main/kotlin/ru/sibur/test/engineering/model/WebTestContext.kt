package ru.sibur.test.engineering.model

import org.assertj.core.api.SoftAssertions
import org.openqa.selenium.Cookie
import org.openqa.selenium.WebDriver
import ru.sibur.test.engineering.utils.downloads.DownloadsUtility
import ru.sibur.test.engineering.properties.CommonProperties
import ru.sibur.test.engineering.utils.BaseWebUtility
import ru.sibur.test.engineering.SeleniumFactory
import ru.sibur.test.engineering.properties.WebProperties
import ru.sibur.test.engineering.utils.BaseUtility

data class WebTestContext(
    var webProperties: WebProperties? = null,
    var wd: WebDriver? = null,
    var cookie: Cookie? = null,
    var softly: SoftAssertions? = null,
    var commonProperties: CommonProperties? = null,
    var baseUtility: BaseUtility? = null,
    var seleniumFactory: SeleniumFactory? = null,
    var baseWebUtility: BaseWebUtility? = null,
    var browserName: String? = null,
    var osName: String? = null,
    var firstTest: String? = null,
    var webTestContext: WebTestContext? = null,
    var downloads: DownloadsUtility? = null,)