package ru.sibur.test.engineering.utils.downloads

import java.nio.file.Path

//
interface DownloadsUtility: AutoCloseable {
    fun localDir(): Path
}