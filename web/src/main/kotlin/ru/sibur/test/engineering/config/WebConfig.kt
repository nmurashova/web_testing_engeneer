package ru.sibur.test.engineering.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("ru.sibur.test.engineering.web")
class WebConfig