package ru.sibur.test.engineering.utils

import org.openqa.selenium.Keys
import ru.sibur.test.engineering.model.WebTestContext
import ru.sibur.test.engineering.pages.MainPage
import ru.sibur.test.engineering.pages.MainPage.Companion.PAGE_TITLE
import ru.sibur.test.engineering.pages.MainPage.Companion.SEARCH_CSS
import io.qameta.allure.Step
import ru.sibur.test.engineering.pages.MainPage.Companion.SEARCH_RESULT

class MainPageUtility(_webTestContext: WebTestContext,
                      _basePoint: BaseWebUtility) : BaseWebUtility(_webTestContext) {

    init {
        this.basePoint = _basePoint
    }

    fun searchFieldFill(city: String) {
        waitPresenceByCss(SEARCH_CSS, 5).click()
        waitPresenceByCss(SEARCH_CSS, 5).sendKeys(city, Keys.ENTER)
    }

    fun selectCity(city: String) {
        waitPresenceByXPath(mainPage.getXPathForSelectCity(city), 5).click()
    }

    fun selectPopularCity(city: String) {
        waitPresenceByXPath(mainPage.getXPathForSelectPopularCity(city), 5).click()
    }

    fun getHeaderOnPopularPage(): String {
        return waitPresenceByXPath(MainPage.PAGE_TITLE, 5).text
    }

    fun selectSubnavMenuItem(item: String) {
        waitPresenceByXPath(mainPage.getXPathForSelectSubnavMenuItem(item), 5).click()
    }

    @Step("Выбор выпадающего списка")
    fun searchDropDownMenu() {
        waitPresenceByXPath(mainPage.getXpathForDropDownMenu(), 5).click()
    }
    @Step("Выбор значения Неделя из выпадающего списка")
    fun selectValue(value : String) {
        waitPresenceByXPath(mainPage.getXpathForValue(value), 5).click()
    }

    @Step("Проверить результат отображения прогноза погоды на неделю")
    fun checkResult() {
        waitPresenceByXPath(mainPage.getXpathForResult(), 5).click()
    }

    @Step("Получить значение String")
    fun getValueResult(): String {
        return waitPresenceByXPath(SEARCH_RESULT, 5).text
    }

}