package ru.sibur.test.engineering.utils.downloads

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class LocalDownloadsUtility(path: String): DownloadsUtility {
    val path: Path = Paths.get(path)

    init {
        Files.createDirectories(this.path)
    }

    override fun localDir(): Path = path

    override fun close() {
        path.toFile().deleteRecursively()
    }
}