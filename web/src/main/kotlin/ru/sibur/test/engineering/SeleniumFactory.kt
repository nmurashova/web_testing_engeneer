package ru.sibur.test.engineering

//import org.openqa.selenium.edge.EdgeOptions
import com.microsoft.edge.seleniumtools.EdgeOptions
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.ie.InternetExplorerOptions
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.logging.LoggingPreferences
import org.openqa.selenium.remote.BrowserType.*
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import ru.sibur.test.engineering.model.WebTestContext
import java.lang.System.setProperty
import java.net.MalformedURLException
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.logging.Level

@Component
class SeleniumFactory {
    val log = LoggerFactory.getLogger(javaClass)

    @Value("\${browser.name:$CHROME}")
    private lateinit var browserName: String

    fun getBrowserName() : String {
        return browserName
    }

    fun selectLocalOrServer(webTestContext: WebTestContext): WebDriver {
        val wd: WebDriver

        if (webTestContext.webProperties!!.path.seleniumServer.equals("")) {
            wd = justDriver(webTestContext)
        } else {
            wd = seleniumServer(webTestContext)
        }
        return wd
    }

    /*
    local driver
     */
    fun justDriver(webTestContext: WebTestContext): WebDriver {
        var driver: WebDriver? = null
        val path = webTestContext.webProperties!!.path
        when(browserName) {
            CHROME -> {
                setProperty("webdriver.chrome.driver", path.chromeDriver)
                val option = ChromeOptions()
                option.addArguments("ignore-certificate-errors")

                val chromePrefs = HashMap<String, Any>()

                chromePrefs["profile.default_content_settings.popups"] = 0

                val localDownloadDirectory = System.getProperty("user.home") + "/Downloads/${UUID.randomUUID()}"
                Files.createDirectories(Paths.get(localDownloadDirectory))
                log.info("localDownloadDirectory = $localDownloadDirectory")

                chromePrefs["download.default_directory"] = localDownloadDirectory
                val caps = DesiredCapabilities()

                option.setExperimentalOption("prefs", chromePrefs);
                option.addArguments("download.default_directory")
                option.merge(caps)

                driver = ChromeDriver(option)
            }
            EDGE -> {
                setProperty("webdriver.edge.driver", path.edgeDriver)
                driver = EdgeDriver()
            }
            IE -> {
                setProperty("webdriver.ie.driver", path.ieDriver)
                driver = InternetExplorerDriver()
            }
        }
        driver!!.manage().window().maximize()
        log.warn("init local selenium WEBDRIVER")
        return driver
    }

    internal fun seleniumServer(webTestContext: WebTestContext): WebDriver {
        var desire: URL? = null
        try {
            desire = URL(webTestContext.webProperties!!.path.seleniumServer)
            log.warn("selenium server URL: $desire")
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }

        val logging = LoggingPreferences()
        logging.enable(LogType.BROWSER, Level.ALL)

        when(browserName) {
            IE -> {
                val internetExplorerOptions = InternetExplorerOptions()

                internetExplorerOptions.setCapability(CapabilityType.LOGGING_PREFS, logging)

                internetExplorerOptions.setCapability("browserName", "internet explorer")
                internetExplorerOptions.setCapability("version", "")
                internetExplorerOptions.setCapability("enableVNC", true)
                internetExplorerOptions.setCapability("enableVideo", false)
                internetExplorerOptions.setCapability("ignoreZoomSetting", true)
                //Запуск IE в режиме инкогнито
                internetExplorerOptions.setCapability("ie.forceCreateProcessApi", true)
                internetExplorerOptions.setCapability("ie.browserCommandLineSwitches", "-private")
                //Не использовать системные настройки Proxy
                //Можно задавать отдельные для каждого процесса
                internetExplorerOptions.setCapability("ie.usePerProcessProxy", true)
                //Фокусировка на окне браузера перед выполнением какого-либо действия
                //Поставить true если webdriver очень медленно заполняет форму для ввода
                //например по 1 символу раз в несколько секунд
                internetExplorerOptions.setCapability("requireWindowFocus", false)

                val driver = RemoteWebDriver(desire, internetExplorerOptions)
                driver.manage().window().maximize()

                log.warn("init selenium server WEBDRIVER(Internet Explorer)")
                return driver
            }
            else -> {
                log.warn("init selenium server EDGE WEBDRIVER")

                val testDir = UUID.randomUUID().toString()
                val remoteDownloadDirectory = "C:\\Users\\jenkins\\Downloads\\$testDir"
                val localDownloadDirectory = System.getProperty("user.home") + "/Downloads/$testDir"
                log.info("localDownloadDirectory = {} | remoteDownloadDirectory = {}",
                        localDownloadDirectory, remoteDownloadDirectory)

                val capabilities = DesiredCapabilities()

                capabilities.setCapability(CapabilityType.LOGGING_PREFS, logging)
                capabilities.browserName = browserName
//                capabilities.setCapability("ms:edgeChromium", false)

                val prefs: MutableMap<String, Any> = HashMap()
                prefs["download.default_directory"] = remoteDownloadDirectory
                val edgeOptions = EdgeOptions()
                edgeOptions.setExperimentalOption("prefs", prefs)
                edgeOptions.addArguments("ignore-certificate-errors")
                capabilities.merge(edgeOptions)

                val driver = RemoteWebDriver(desire, edgeOptions)
                driver.manage().window().maximize()

                log.warn("init selenium server WEBDRIVER ($browserName)")
                return driver
            }
        }
    }
}