package ru.sibur.test.engineering.utils

import io.qameta.allure.Allure
import io.qameta.allure.Step
import org.assertj.core.api.SoftAssertions
import org.openqa.selenium.*
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.BrowserType.CHROME
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.slf4j.LoggerFactory
import ru.sibur.test.engineering.model.WebTestContext
import ru.sibur.test.engineering.pages.MainPage
import ru.sibur.test.engineering.properties.WebProperties
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


open class BaseWebUtility (
    _webTestContext: WebTestContext,
    _basePoint: BaseWebUtility? = null
) {
    private val log = LoggerFactory.getLogger(javaClass)

    protected val webTestContext: WebTestContext
    protected val baseUtility: BaseUtility
    protected val webProperties: WebProperties
    val wd: WebDriver
    val softly: SoftAssertions

    protected lateinit var basePoint: BaseWebUtility

    init {
        this.webTestContext = _webTestContext
        this.webProperties = webTestContext.webProperties!!
        this.baseUtility = webTestContext.baseUtility!!
        this.wd = webTestContext.wd!!
        this.softly = webTestContext.softly!!
    }

    companion object {
        val log = LoggerFactory.getLogger(javaClass)

        fun base(webTestContext: WebTestContext):
                BaseWebUtility {
            val baseWebUtility = BaseWebUtility(webTestContext)
            baseWebUtility.basePoint = baseWebUtility
            log.warn("init base baseWebUtility utility.")
            return baseWebUtility
        }
    }

    private var mainPageUtility: MainPageUtility? = null
    fun mainPageUtil(): MainPageUtility {
        if (basePoint.mainPageUtility == null) {
            basePoint.mainPageUtility =
                MainPageUtility(webTestContext, basePoint)
        }
        return basePoint.mainPageUtility!!
    }

    protected val mainPage: MainPage
        get() {
            return MainPage(wd)
        }

    @Step("Collect soft assert items")
    fun assertAll() {
        softly.assertAll()
    }

    @Step("Take Screenshot")
    fun takeScreenshot(name: String? = null) {
        val n = "ScreenShot" + if (name != null) {
            "_${name.replace("\\s".toRegex(), "_")}"
        } else {
            ""
        }

        if (wd is TakesScreenshot) {
            val screenshot = wd.getScreenshotAs(OutputType.FILE)
            screenshot.deleteOnExit()
            Allure.addAttachment(n, "image/jpg", screenshot.inputStream(), ".jpg")
        }
    }

    protected fun wait(seconds: Long): WebDriverWait =
         WebDriverWait(wd, seconds)

    @Step("ожидание кликабельности {element} {seconds} секунд")
    fun waitClickable(element: WebElement, seconds: Long): WebElement =
            wait(seconds).until(ExpectedConditions
                    .elementToBeClickable(element))

    @Step("ожидание кликабельности {element} {seconds} секунд")
    fun waitClickable(element: String, seconds: Long): WebElement =
            wait(seconds).until(ExpectedConditions
                    .elementToBeClickable(By.xpath(element)))

    @Step("ожидание появления в DOM {element} {seconds} секунд")
    fun waitPresenceByXPath(element: String, seconds: Long): WebElement =
        wait(seconds).until(ExpectedConditions
            .presenceOfElementLocated(By.xpath(element)))

    @Step("ожидание появления в DOM {element} {seconds} секунд")
    fun waitPresenceByCss(element: String, seconds: Long): WebElement =
        wait(seconds).until(ExpectedConditions
            .presenceOfElementLocated(By.cssSelector(element)))

    @Step("ожидание появления в DOM {element} {seconds} секунд")
    fun waitPresenceById(element: String, seconds: Long): WebElement =
        wait(seconds).until(ExpectedConditions
            .presenceOfElementLocated(By.id(element)))

    @Step("ожидание появления в DOM nested element {element} {seconds} секунд")
    fun waitPresenceNestedElement(parentElement: WebElement, element: String, seconds: Long): WebElement =
            wait(seconds).until(ExpectedConditions
                    .presenceOfNestedElementLocatedBy(parentElement, By.xpath(element)))

    @Step("move To Element {xpath}")
    fun moveToElement(xpath: String) {
        val act = Actions(wd)
        act.moveToElement(wd.findElement(By.xpath(xpath))).build().perform()
    }

    @Step("move To Element {element}")
    fun moveToElement(element: WebElement) {
        val act = Actions(wd)
        try {
            act.moveToElement(element).build().perform()
        } catch (exc: ElementNotInteractableException) {
            val executor = wd as JavascriptExecutor
            executor.executeScript("arguments[0].scrollIntoView(true);", element)
        }
    }

    @Step("ожидание появления в DOM {xpath} с текстом {text} {seconds} секунд")
    fun waitTextToBePresentInElementLocated(xpath: String, text: String, seconds: Long): Boolean =
            wait(seconds).until(ExpectedConditions
                    .textToBePresentInElementLocated(By.xpath(xpath), text))

    @Step("ожидание появления в DOM {xpath} с текстом {text} {seconds} секунд")
    fun waitTextContentToBeInElementLocated(xpath: String, text: String, seconds: Long): Boolean =
            wait(seconds).until(ExpectedConditions
                    .attributeToBe(By.xpath(xpath), "textContent", text))

    fun waitNumberOfElementsToBeMoreThan(element: String, number: Int, seconds: Long): List<WebElement> =
            wait(seconds).until(ExpectedConditions
                    .numberOfElementsToBeMoreThan(By.xpath(element), number))

    @Step("ожидание появления в DOM {element} {seconds} секунд")
    fun waitPresenceOfElements(element: String, seconds: Long): List<WebElement> {
        return try {
            wait(seconds).until(ExpectedConditions
                    .presenceOfAllElementsLocatedBy(By.xpath(element)))
        } catch (exc: TimeoutException)  {
            emptyList()
        }
    }

    @Step("ожидание появления в DOM nestedElement {parentElement}/{nestedElement} {seconds} секунд")
    fun waitPresenceOfNestedElement(parentElement: String, nestedElement: String, seconds: Long): WebElement =
            wait(seconds).until(ExpectedConditions
                    .presenceOfNestedElementLocatedBy(By.xpath(parentElement), By.xpath(nestedElement)))

    @Step("wait Visibility {locator} {seconds} секунд")
    fun waitVisibility(locator: By, seconds: Long): WebElement =
        wait(seconds).until(ExpectedConditions
            .visibilityOfElementLocated(locator))

    @Step("wait Visibility {element} {seconds} секунд")
    fun waitVisibility(element: WebElement, seconds: Long): WebElement =
            wait(seconds).until(ExpectedConditions
                    .visibilityOf(element))

    @Step("wait To Be Present In Element Located {locator} {seconds} секунд")
    fun waitToBePresentInElementLocated(locator: By, text: String, seconds: Long): Boolean =
            wait(seconds).until(ExpectedConditions
                    .textToBePresentInElementLocated(locator, text))

    @Step("ожидание пропадания {locator} {seconds} секунд")
    fun waitInvisible(locator: By, seconds: Long): Boolean =
        wait(seconds).until(ExpectedConditions
            .invisibilityOfElementLocated(locator))

    @Step("ожидание пропадания {element} {seconds} секунд")
    fun waitInvisible(element: WebElement, seconds: Long): Boolean =
            wait(seconds).until(ExpectedConditions
                    .invisibilityOf(element))

    @Step("wait Visibility Of All Elements {xpath} {seconds} секунд")
    fun waitVisibilityOfAllElements(xpath: String, seconds: Long): List<WebElement> =
            wait(seconds).until(ExpectedConditions
                    .visibilityOfAllElementsLocatedBy(By.xpath(xpath)))

    @Step("wait Visibility Of All Elements {elements} {seconds} секунд")
    fun waitVisibilityOfAllElements(elements: List<WebElement>, seconds: Long): List<WebElement> =
            wait(seconds).until(ExpectedConditions
                    .visibilityOfAllElements(elements))

    @Step("wait Elements Refreshed {elements} {seconds} секунд")
    fun waitElementsRefreshed(elements: List<WebElement>, seconds: Long) : Boolean {
            try {
                val end = System.currentTimeMillis() + seconds*1000
                while (System.currentTimeMillis() < end) {
                    if (elements.isNotEmpty()) elements.get(0).text
                    Thread.sleep(500)
                }
            } catch (exc: StaleElementReferenceException) {
                log.info("Elements refreshed")
                return true
            }
        return false
    }

    @Step("открываем базовый url {url}")
    fun open(url: String) {
        wd.get(url)
    }

    @Step("скролим до {element}")
    fun scrollToElement(element: WebElement) =
        (wd as JavascriptExecutor)
            .executeScript("arguments[0].scrollIntoView(true);", element)

    @Step("скролим до {element}")
    fun scrollToElement(xpath: String) {
        val element = waitPresenceByXPath(xpath, 5)
        (wd as JavascriptExecutor)
                .executeScript("arguments[0].scrollIntoView(true);", element)
    }

    @Step("скролим до координат x = {x} y = {y}")
    fun scrollCoordinates(x: Int, y: Int) =
        (wd as JavascriptExecutor)
            .executeScript("javascript:window.scrollBy($x,$y)")

    @Step("Click on element with js")
    fun clickWithJs(element: WebElement) {
        val executor = wd as JavascriptExecutor
        executor.executeScript("arguments[0].click();", element)
    }

    @Step("скрол - до подвала страницы")
    fun scrollToHeight() =
        (wd as JavascriptExecutor)
            .executeScript("window.scrollTo(5, document.body.scrollHeight)")

    @Step("форматируем стрингу {date} шаблона \"dd.MM.yyyy\" в объект LocalDateTime")
    fun stringParseLocalDate(date: String): LocalDateTime {
        val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
        val lD = LocalDate.parse(date, formatter)
        return lD.atStartOfDay()
    }

    @Step("форматируем стрингу {date} шаблона \"dd MMMM yyyy 'в' HH:mm\" в объект LocalDateTime")
    fun stringParseLocalDateTime(date: String): LocalDateTime {
        val formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy 'в' HH:mm")
        return LocalDateTime.parse(date, formatter)
    }

    @Step("находим {element}, кликаем, проверяем заполнено ли поле необходимым текстом {text} , если нет то очищаем и заполняем")
    fun sendKeys(element: WebElement, text: String) {
        with (element) {
            click()
            if (this.text != text) {
                clear()
                sendKeys(text)
            }
        }
    }

    @Step("находим {element}, очищаем и ждем, когда поле будет очищено")
    fun waitInputBeClear(element: WebElement) : WebElement {
        val cap = (wd as RemoteWebDriver).capabilities
        waitClickable(element, 5).click()
        if (cap.browserName.equals(CHROME, true))
            element.sendKeys(Keys.chord(Keys.COMMAND,"a"), Keys.BACK_SPACE)
        else
            element.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE))
        try {
            wait(5).until(ExpectedConditions
                    .attributeToBe(element, "value", ""))
        } catch (exc: TimeoutException) {
            while (element.getAttribute("value") != "") {
                element.click()
                element.sendKeys(Keys.END)
                element.sendKeys(Keys.BACK_SPACE)
            }
        }

        return  element
    }

    @Step("находим {xpath}, очищаем и ждем, когда поле будет очищено")
    fun waitInputBeClear(xpath: String) : WebElement {
        val cap = (wd as RemoteWebDriver).capabilities
        val element = waitPresenceByXPath(xpath, 5)
        scrollToElement(xpath)
        waitClickable(xpath, 5).click()
        if (cap.browserName.equals(CHROME, true))
            element.sendKeys(Keys.chord(Keys.COMMAND,"a"), Keys.BACK_SPACE)
        else
            element.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE))
        try {
            wait(5).until(ExpectedConditions
                    .attributeToBe(element, "value", ""))
        } catch (exc: TimeoutException) {
            while (element.getAttribute("value") != "") {
                element.click()
                element.sendKeys(Keys.END)
                element.sendKeys(Keys.BACK_SPACE)
            }
        }

        return  element
    }

    @Step("находим с ожиданием кликабельности {element}, кликаем, проверяем заполнено ли поле необходимым текстом {text}, если нет то очищаем и заполняем")
    fun sendKeysWithWait(element: WebElement, text: String) {
        with (waitClickable(element, 3)) {
            click()
            if (this.text != text) {
                clear()
                sendKeys(text)
            }
        }
    }

    fun getPlatform() : String {
        val cap = (wd as RemoteWebDriver).capabilities
        return cap.platform.toString()
    }

    fun quitWd() {
        wd.quit()

        if (webTestContext.downloads != null) {
            webTestContext.downloads!!.close()
            webTestContext.downloads = null
        }
    }

}