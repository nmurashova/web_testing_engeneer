package ru.sibur.test.engineering.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.context.annotation.PropertySources

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "web", ignoreInvalidFields = true)
@PropertySources(
    PropertySource(value = ["classpath:web/driver.yml"], ignoreResourceNotFound = true,
        encoding = "UTF-8", factory = YamlPropertySourceFactory::class),
    PropertySource(value = ["classpath:web/driver-\${webdriver.user}.yml"], ignoreResourceNotFound = true,
        encoding = "UTF-8", factory = YamlPropertySourceFactory::class))
class WebProperties {

    val path = Path()
    val sftp = Sftp()

    class Path {
        lateinit var chromeDriver: String
        lateinit var edgeDriver: String
        lateinit var ieDriver: String
        lateinit var geckoDriver: String
        lateinit var seleniumServer: String
    }

    class Sftp {
        var user = "jenkins"
        var password = "123"
        var hostname = "10.2.65.13" // взять из конфига
    }
}