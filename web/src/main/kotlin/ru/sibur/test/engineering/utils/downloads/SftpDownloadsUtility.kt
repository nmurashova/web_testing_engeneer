package ru.sibur.test.engineering.utils.downloads

import ru.sibur.test.engineering.utils.command.SftpClient
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class SftpDownloadsUtility(
        val remoteUser: String,
        val remotePassword: String,
        val remoteHost: String,
        val remotePath: String,
        val localPath: String): DownloadsUtility {

    init {
        Files.createDirectories(Paths.get(localPath))

        SftpClient(remoteHost, 22, remoteUser, remotePassword).use {
            it.makeDir(remotePath)
        }
    }

    override fun localDir(): Path {
        SftpClient(remoteHost, 22, remoteUser, remotePassword).use {
            it.downloadDir(remotePath, localPath)
        }

        return Paths.get(localPath)
    }

    override fun close() {
        SftpClient(remoteHost, 22, remoteUser, remotePassword).use {
            it.removeDir(remotePath)
        }

        File(localPath).deleteRecursively()
    }
}