package ru.sibur.test.engineering.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class MainPage(webdriver: WebDriver) {

    init {
        PageFactory.initElements(webdriver, this)
    }

    companion object {
        const val SEARCH_XPATH =  "//input[@type=\"search\"]"
        const val SEARCH_CSS =  "input[type='search']"
       // const val SEARCH_CITY_IN_MENU = "//div[@class=\"city-title\" and @text = \"Пенза]"

        const val SEARCH_DROPDOWNMENU = "//*[@aria-controls = \"curr-weather-dropdown-menu-more\"]"
        const val SEARCH_WEEK = "//*[contains(text(), \"Неделя\")]"
        const val SEARCH_RESULT = "//h1[contains(text(), 'неделю')]"
        //test2
        // const val SEARCH_POPULAR_CITY = "//div[@class="cities-popular"]//a[text()="Барнаул"]"
        const val PAGE_TITLE = "//div[@class=\"page-title\"]"
        //const val TEN_DAYS_BUTTON = "//a[contains(@class,"subnav-menu-item")][contains(@title,"10 дней")]"
        //const val PAGE_TITLE_TEN_DAYS = "//div[@class="page-title"]/h1[contains(text(),"10 дней")]"
    }

    @FindBy(css = SEARCH_CSS)
    lateinit var searchInput: WebElement

    fun getXPathForSelectCity(city: String): String {
        return "//div[@class=\"city-title \" and text() = \"$city\"]"
    }

    fun getXpathForDropDownMenu() : String {
        return "//*[@aria-controls = \"curr-weather-dropdown-menu-more\"]"
    }

    fun getXpathForValue(value : String) : String {
        return "//*[contains(text(), \"$value\")]"
    }

    fun getXpathForResult() : String {
        return "//h1[contains(text(), 'неделю')]"
    }

    fun getXPathForSelectPopularCity(city: String): String {
        return "//div[@class=\"cities-popular\"]//a[text()=\"$city\"]"
    }

    fun getXPathForSelectSubnavMenuItem(item: String): String {
        return "//a[contains(@class,\"subnav-menu-item\")][contains(@title,\"$item\")]"
    }
}