package ru.sibur.test.engineering.utils.command

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.ChannelSftp.LsEntry
import com.jcraft.jsch.ChannelSftp.LsEntrySelector
import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.io.path.pathString

class SftpClient(val host: String, val port: Int, username: String, password: String) : AutoCloseable {
    private val log = LoggerFactory.getLogger(javaClass)

    val session: Session
    val channelSftp: ChannelSftp

    init {
        session = JSch().getSession(username, host, port)
        session.setPassword(password)
        session.setConfig("StrictHostKeyChecking", "no")
        session.connect()

        channelSftp = session.openChannel("sftp") as ChannelSftp
        channelSftp.connect()
    }

    private fun normalizePath(remotePath: String): String =
            if (remotePath.matches("^[A-Za-z]:\\\\.*".toRegex())) {
                "/" + remotePath.replace("\\", "/")
            } else {
                remotePath
            }

    fun makeDir(remotePath: String) {
        channelSftp.mkdir(normalizePath(remotePath))
    }

    fun downloadDir(remotePath: String, localPath: String) {
        val normalizedRemotePath = normalizePath(remotePath)
        val files = ArrayList<String>()

        channelSftp.ls(normalizedRemotePath) { e: LsEntry ->
            if (e.attrs.isReg) {
                files.add(e.filename)
            } else {
                throw IllegalStateException(
                        "Невозможно загрузить директорию с нерегулярным файлом: ${normalizedRemotePath}/${e.filename}")
            }

            LsEntrySelector.CONTINUE
        }

        files.forEach { file: String ->
            val remoteFile = "${normalizedRemotePath}/${file}"
            val localFile = Paths.get(localPath, file)

            log.info("Copying: {}:{}{} -> {}", host, port, remoteFile, localFile)
            Files.deleteIfExists(localFile)
            channelSftp.get(remoteFile, localFile.pathString)
        }
    }

    fun removeDir(remotePath: String) {
        val normalizedRemotePath = normalizePath(remotePath)
        val files = ArrayList<String>()

        channelSftp.ls(normalizedRemotePath) { e: LsEntry ->
            if (e.attrs.isReg) {
                files.add(e.filename)
            } else {
                throw IllegalStateException(
                        "Невозможно удалить директорию с нерегулярным файлом: ${normalizedRemotePath}/${e.filename}")
            }

            LsEntrySelector.CONTINUE
        }

        files.forEach { file: String ->
            val remoteFile = "${normalizedRemotePath}/${file}"

            log.info("Deleting: {}:{}{}", host, port, remoteFile)
            channelSftp.rm(remoteFile)
        }

        channelSftp.rmdir(normalizedRemotePath)
    }

    override fun close() {
        channelSftp.disconnect()
        session.disconnect()
    }
}