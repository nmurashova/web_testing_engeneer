package ru.sibur.test.svt.api.model.keycloak

data class KeycloakUserInfoResponse(
    val sub: String,
    val city: String?,
    val firstName: String?,
    val lastName: String?,
    val middleName: String?,
    val employeeNumber: String?,
    val company: String?,
    val email: String?,
    val phone: String?,
    val position: String?
)
