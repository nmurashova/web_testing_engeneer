package ru.sibur.test.engineering.api.config

import io.grpc.ManagedChannel
import io.grpc.netty.GrpcSslContexts
import io.grpc.netty.NegotiationType
import io.grpc.netty.NettyChannelBuilder
import net.devh.boot.grpc.client.config.GrpcChannelsProperties
import net.devh.boot.grpc.client.inject.StubTransformer
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import ru.sibur.test.svt.api.keycloak.AuthKeycloakClientService
import ru.sibur.test.svt.api.utils.interceptors.AuthTokenProvideInterceptor
import ru.sibur.test.engineering.properties.CommonProperties
import java.io.ByteArrayInputStream

@Configuration
@ComponentScan("ru.sibur.test.svt.api")
class GrpcConfig(
    private val commonProperties: CommonProperties,
    private val grpcChannelsProperties: GrpcChannelsProperties
) {
    private val log = LoggerFactory.getLogger(javaClass)

    @Autowired
    private lateinit var authKeycloackClientService: AuthKeycloakClientService

    @Bean
    fun call(): StubTransformer {
        return StubTransformer { name, stub ->
            when (name) {
                "auth-server" -> {
                    log.info("Настраиваю клиента сервиса авторизации {}", name)
                    stub.getChannel()
                    stub
                }
                else -> {
                    log.info("Настраиваю обработчик авторизации для клиента {} ({})", name, stub.javaClass.simpleName)
                    stub.withInterceptors(AuthTokenProvideInterceptor(
                            authKeycloackClientService,
                            commonProperties.web.username,
                            commonProperties.web.password))
                }
            }
        }
    }

    @Bean("app-channel")
    fun siauthChannel() = createChannel()

    private fun createChannel(serverName: String = "app-server"): ManagedChannel {

        val grpcChannelProperties = grpcChannelsProperties.client[serverName]!!
        val bytes = with(this::class.java.classLoader.getResourceAsStream("certificates/rootCA.pem")!!) {
            readAllBytes()
        }

        val sslContext = GrpcSslContexts.forClient()
                .trustManager(ByteArrayInputStream(bytes))
                .build()

        val channel = NettyChannelBuilder
                .forTarget(grpcChannelProperties.address.toString())
                .negotiationType(NegotiationType.TLS)
                .sslContext(sslContext)
                .build()

        return channel
    }
}