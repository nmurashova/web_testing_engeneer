package ru.sibur.test.svt.api.keycloak

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.sibur.test.svt.api.model.keycloak.KeycloakJWTRequest
import ru.sibur.test.svt.api.model.keycloak.KeycloakJWTResponse
import ru.sibur.test.svt.api.model.keycloak.KeycloakRBACRolesResponse
import ru.sibur.test.svt.api.model.keycloak.KeycloakUserInfoResponse
import ru.sibur.test.svt.api.utils.keycloack.EmployeeKeycloakClient

@Service
class AuthKeycloakClientService {
    private val log = LoggerFactory.getLogger(javaClass)

    @Autowired
    private lateinit var employeeKeycloakClient: EmployeeKeycloakClient

    fun login(login: String, password: String): String {
        val response = employeeKeycloakClient.createJWT(
            KeycloakJWTRequest(login = login, password = password)
        )
        val responseStatus = response.statusCode
        if (responseStatus.is2xxSuccessful) {
            val responseBody = response.body
//            log.info("create keycloak JWT $responseBody")
            return responseBody?.get("access_token")!!
        } else {
            throw RuntimeException("Keycloak Token not received status code ${responseStatus.value()}")
        }
    }

    fun getUserInfo(keycloakJWTResponse: KeycloakJWTResponse): KeycloakUserInfoResponse {
        val userInfoResponse = employeeKeycloakClient.getUserInfo(token = "Bearer ${keycloakJWTResponse.accessToken}")
        val responseStatus = userInfoResponse.statusCode
        if (responseStatus.is2xxSuccessful) {
            val responseBody = userInfoResponse.body
            log.info("get keycloak user info $responseBody")
            return KeycloakUserInfoResponse(
                sub = responseBody?.get("sub")!!,
                city = responseBody["city"],
                firstName = responseBody["first_name"],
                lastName = responseBody["last_name"],
                middleName = responseBody["middle_name"],
                employeeNumber = responseBody["employee_number"],
                company = responseBody["company"],
                email = responseBody["email"],
                phone = responseBody["phone"],
                position = responseBody["position"]
            )
        } else {
            throw RuntimeException("Keycloak UserInfo not received status code ${responseStatus.value()}")
        }
    }

    fun getUserRoles(keycloakJWTResponse: KeycloakJWTResponse): KeycloakRBACRolesResponse {
        val userRolesResponse = employeeKeycloakClient.getUserRoles(token = "Bearer ${keycloakJWTResponse.accessToken}")
        val responseStatus = userRolesResponse.statusCode
        if (responseStatus.is2xxSuccessful) {
            val responseBody = userRolesResponse.body
            log.info("get keycloak user info $responseBody")
            return KeycloakRBACRolesResponse(
                sub = responseBody?.get("sub")!! as String,
                roles = responseBody["roles"] as MutableList<String>
            )
        } else {
            throw RuntimeException("Keycloak UserRoles not received status code ${responseStatus.value()}")
        }
    }

}