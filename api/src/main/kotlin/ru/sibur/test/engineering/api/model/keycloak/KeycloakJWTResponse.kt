package ru.sibur.test.svt.api.model.keycloak

data class KeycloakJWTResponse(
    val accessToken: String,
    val refreshToken: String
)
