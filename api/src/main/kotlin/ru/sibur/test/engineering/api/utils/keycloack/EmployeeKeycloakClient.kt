package ru.sibur.test.svt.api.utils.keycloack

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import ru.sibur.test.svt.api.config.FeignConfig
import ru.sibur.test.svt.api.model.keycloak.KeycloakJWTRequest
import ru.sibur.test.svt.api.model.keycloak.KeycloakRefreshJWTRequest

@FeignClient(
    name = "employeeKeycloakClient",
    url = "\${test.keycloak.url}",
    configuration = [FeignConfig::class]
)

interface EmployeeKeycloakClient {
    @RequestMapping(
        method = [RequestMethod.POST],
        value = ["/auth/authn/jwt/create"],
        consumes = ["application/json"]
    )
    fun createJWT(
        body: KeycloakJWTRequest
    ): ResponseEntity<MutableMap<String, String>>

    @RequestMapping(
        method = [RequestMethod.POST],
        value = ["/auth/authn/jwt/refresh"],
        consumes = ["application/json"]
    )
    fun refreshJWT(
        body: KeycloakRefreshJWTRequest
    ): ResponseEntity<MutableMap<String, String>>

    @RequestMapping(
        method = [RequestMethod.GET],
        value = ["/auth/userinfo"],
        consumes = ["application/json"]
    )
    fun getUserInfo(
        @RequestHeader("Authorization") token: String
    ): ResponseEntity<MutableMap<String, String>>

    @RequestMapping(
        method = [RequestMethod.GET],
        value = ["/auth/authz/rbac/roles"],
        consumes = ["application/json"]
    )
    fun getUserRoles(
        @RequestHeader("Authorization") token: String
    ): ResponseEntity<MutableMap<String, Any>>
}
