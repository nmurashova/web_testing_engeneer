package ru.sibur.test.svt.api.model.keycloak

data class KeycloakJWTRequest(
    val login: String,
    val password: String
)
