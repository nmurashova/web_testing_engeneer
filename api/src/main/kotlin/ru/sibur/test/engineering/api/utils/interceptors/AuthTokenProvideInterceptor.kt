package ru.sibur.test.svt.api.utils.interceptors

import io.grpc.*
import io.qameta.allure.Step
import ru.sibur.test.svt.api.keycloak.AuthKeycloakClientService

class AuthTokenProvideInterceptor(
        private val authKeycloackClientService: AuthKeycloakClientService,
        private val username : String,
        private val password : String
): ClientInterceptor {

    @Step("добавление хедера авторизации")
    override fun <ReqT, RespT> interceptCall(methodDescriptor: MethodDescriptor<ReqT, RespT>,
                                             callOptions: CallOptions,
                                             channel: Channel
    ): ClientCall<ReqT, RespT> {
        return object : ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(
                channel.newCall(methodDescriptor, callOptions)) {

            override fun start(responseListener: Listener<RespT>, headers: Metadata) {
                val token = authKeycloackClientService.login(username, password)
                headers.put(Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER), "Bearer $token")
                super.start(responseListener, headers)
            }
        }
    }
}