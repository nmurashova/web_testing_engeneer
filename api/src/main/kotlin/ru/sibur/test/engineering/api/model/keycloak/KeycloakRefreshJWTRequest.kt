package ru.sibur.test.svt.api.model.keycloak

class KeycloakRefreshJWTRequest {
    data class KeycloakRefreshJWTRequest(
        val refreshToken: String
    )

}