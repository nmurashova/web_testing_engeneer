package ru.sibur.test.svt.api.model.keycloak

data class KeycloakRBACRolesResponse(
    val sub: String,
    val roles: MutableList<String>
)
