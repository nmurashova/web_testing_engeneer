package ru.sibur.test.svt.api.config

import com.google.gson.GsonBuilder
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import feign.Logger
import feign.Request
import feign.Response
import feign.Util
import org.slf4j.LoggerFactory
import java.io.IOException
import java.nio.charset.StandardCharsets

class AllureLogger : Logger() {
    private val log = LoggerFactory.getLogger(javaClass)

    override fun log(configKey: String, format: String, vararg args: Any) {}

    override fun logRequest(configKey: String, logLevel: Level, request: Request) {
        log.info(">>>>> Request URL: ${request.url()}")
        log.info(">>>>> Request Method: ${request.httpMethod()}")
        val headers = request
            .headers()
            .map { (key, value) ->
                "$key: ${", ".plus(value)}"
            }.joinToString(separator = "\n")
        log.info(">>>>> Request Header:\n${headers}")
        if (request.body() != null) {
            val bodyContent = formatBody(String(request.body(), request.charset()))
            log.info(">>>>> Request Body:\n${bodyContent}")
        }
    }

    override fun logIOException(configKey: String,
                                logLevel: Level,
                                ioe: IOException,
                                elapsedTime: Long) = ioe

    override fun logAndRebufferResponse(configKey: String,
                                        logLevel: Level,
                                        response: Response,
                                        elapsedTime: Long): Response {
        if (response.body() != null) {
            val body = Util.toString(response.body().asReader(StandardCharsets.UTF_8))
            val bodyContent = formatBody(body)
            log.info(">>>>> Response STATUS: ${response.status()}")
            val headers = response
                .headers()
                .map { (key, value) ->
                    "$key: ${", ".plus(value)}"
                }.joinToString(separator = "\n")
//            log.info(">>>>> Response Header:\n${headers}")
//            log.info(">>>>> Response Body:\n${bodyContent}")
            return response.toBuilder().body(body.toByteArray()).build()
        } else {
            return response
        }
    }

    fun formatBody(body: String): String {
        val parser = JsonParser()
        try {
            val json = parser.parse(body)
            val gson = GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .create()

            return gson.toJson(json)
        } catch (ex: JsonParseException) {
            return body
        }
    }
}