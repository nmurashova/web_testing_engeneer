package ru.sibur.test.svt.api.config

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import feign.Client
import feign.Logger
import feign.httpclient.ApacheHttpClient
import org.apache.http.client.config.RequestConfig
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.conn.ssl.TrustSelfSignedStrategy
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.ssl.SSLContextBuilder
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableFeignClients(basePackages = ["ru.sibur.test.svt.api.utils"])
class FeignConfig {
 //  private val log = LoggerFactory.getLogger(javaClass)

    @Bean
    fun feignLoggerLevel(): Logger.Level? {
        return Logger.Level.FULL
    }

    @Bean
    fun feignClient(): Client {
        return ApacheHttpClient(getHttpClient())
    }

    @Bean
    fun logger(): Logger = AllureLogger()

    @Bean
    fun jacksonObjectMapper(): ObjectMapper {
        val mapper = ObjectMapper()
        val module = KotlinModule()
        mapper.setSerializationInclusion(JsonInclude.Include.ALWAYS)
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true)
        mapper.registerModule(module)
//        mapper.registerModule(Jdk8Module())
        return mapper
    }

    private fun getHttpClient(): CloseableHttpClient {
        val timeout = 10000
        try {
            val sslContext = SSLContextBuilder.create()
                .loadTrustMaterial(TrustSelfSignedStrategy()).build()
            val config = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout)
                .build();
            return HttpClientBuilder
                .create()
                .useSystemProperties()
                .setDefaultRequestConfig(config)
                .setSSLContext(sslContext)
                .setSSLHostnameVerifier(NoopHostnameVerifier())
                .build();
        } catch (e: Exception) {
            throw RuntimeException()
        }
    }
}