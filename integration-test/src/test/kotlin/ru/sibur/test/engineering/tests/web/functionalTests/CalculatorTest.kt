package ru.sibur.test.engineering.tests.web.functionalTests

import org.assertj.core.util.Strings
import org.junit.Assume
import org.junit.jupiter.api.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvFileSource
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource


@Tags(value = [Tag("web"), Tag("dev")])
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CalculatorTest {

    private var calculator =  Calculator()

    @BeforeAll
    fun setup() {
        println("@BeforeAll executed")
    }


    @BeforeEach
    fun setupThis() {
        println("@BeforeEach executed")
    }

    @Test
    fun testCalcOne() {
        println("======TEST ONE EXECUTED=======")
        Assertions.assertEquals(4, calculator.add(2, 2))
    }

    @Test
    fun testCalcTwo() {
        println("======TEST TWO EXECUTED=======")
        Assertions.assertEquals(5, calculator.add(2, 4))
    }



    @Test
    @Disabled
    fun testCalcThree() {
        println("======TEST THREE EXECUTED=======")

        val strings = arrayOf("January", "February", "March")

        Assume.assumeFalse(strings.isEmpty())

        Assertions.assertEquals(6, calculator.add(2, 4))
    }


    @ParameterizedTest
    @ValueSource(strings = ["January", "February", "March"])
    fun testCalcFour(input: String) {
        println("======TEST FOUR EXECUTED=======")

        Assertions.assertEquals(7, input.length)
    }

    @ParameterizedTest
    @CsvFileSource(resources = ["/web/calcTest_1.csv"], numLinesToSkip = 1, delimiter = ';')
    fun testCalcFive(input: String) {
        println("======TEST FIVE EXECUTED=======")

        Assertions.assertEquals(7, input.length)
    }

    @ParameterizedTest // сложение
    @CsvFileSource(resources = ["/web/calcTest_3_int.csv"], numLinesToSkip = 1, delimiter = ';')
    fun testHomework3(sum: Int, num_1: Int, num_2: Int) {
        Assertions.assertEquals(sum, calculator.add(num_1, num_2))
        println("" + sum + " = " + calculator.add(num_1, num_2))
        //println("Число из первого столбца " + sum + " = сумме чисел из второго и третьего столбца " + calculator.add(num_1, num_2))
    }

    @Test
    fun testCalcSeven() {
        println("======TEST SEVEN EXECUTED=======")
        Assertions.assertEquals(4, calculator.sub(8, 4))
    }

    @ParameterizedTest // сложение строк
    @CsvFileSource(resources = ["/web/calcTest_4_string.csv"], numLinesToSkip = 1, delimiter = ';')
    fun testHomework4(sum: String, num_1: String, num_2: String) {
        Assertions.assertEquals(sum, calculator.text(num_1, num_2))
        println("" + sum + " = " + calculator.text(num_1, num_2))

            }

    @ParameterizedTest // деление
    @CsvFileSource(resources = ["/web/calcTest_3_int.csv"], numLinesToSkip = 1, delimiter = ';')
    fun testHomework4(sum : Int, num_1: Float, num_2: Float, multip : Int, divis : Float) {
        Assertions.assertEquals(divis, calculator.div(num_1, num_2))
        println("" + divis + " = " + calculator.div(num_1, num_2))
    }

    @ParameterizedTest // умножение
    @CsvFileSource(resources = ["/web/calcTest_3_int.csv"], numLinesToSkip = 1, delimiter = ';')
    fun testHomework5 (sum: Int, num_1: Int, num_2: Int, multip: Int) {
        Assertions.assertEquals(multip, calculator.mult(num_1, num_2))
        println("" + multip + " = " + calculator.mult(num_1, num_2))
    }

//    @ParameterizedTest(name = "Calculator test")
//    @CsvSource({"apple, banana"})
//    fun testHomework6(a : String, b : String) {
//        val result = a + b;
//        Assertions.assertEquals(result, calculator.text(a, b))
//    }

    @AfterEach
    fun tearThis() {
        println("@AfterEach executed")
    }

    @AfterAll
    fun tear() {
        println("@AfterAll executed")
    }
}