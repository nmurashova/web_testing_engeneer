package ru.sibur.test.engineering.tests.base

import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.Extensions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import ru.sibur.test.engineering.CommonApplication
import ru.sibur.test.engineering.tests.IntegrationApplication
import ru.sibur.test.engineering.tests.extensions.WebTestExtension

@SpringBootTest(classes = [CommonApplication::class, IntegrationApplication::class])
@Extensions(value = [
    ExtendWith(SpringExtension::class),
    ExtendWith(WebTestExtension::class)
])
class WebTestBase {

}