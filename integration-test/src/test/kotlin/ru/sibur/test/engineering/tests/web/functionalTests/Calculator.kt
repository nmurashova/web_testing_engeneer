package ru.sibur.test.engineering.tests.web.functionalTests

import org.junit.jupiter.api.Tags
import org.junit.jupiter.api.Tag

@Tags(value = [Tag("web"), Tag("dev")])
class Calculator {

    fun add(a: Int, b: Int): Int {
        return a + b
    }

    fun sub(a: Int, b: Int): Int {
        return a - b
    }

    fun mult(a: Int, b: Int): Int {
        return a * b
    }

    fun div(a: Float, b: Float): Float {
        return a / b
    }

    fun text(a: String, b: String): String {
        return a + b
    }

}