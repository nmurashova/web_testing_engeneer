package ru.sibur.test.engineering.tests.extensions

import org.junit.jupiter.api.extension.*
import org.slf4j.LoggerFactory

class ApiTestExtension :
    BeforeAllCallback,
    BeforeEachCallback,
    AfterAllCallback,
    AfterEachCallback {
    private val log = LoggerFactory.getLogger(javaClass)

    override fun beforeAll(context: ExtensionContext?) {
        println("^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^" +
            "\n===>>> START TEST ${context!!.testClass?.get()} <<<===")
    }

    override fun beforeEach(context: ExtensionContext?) {
        val displayName = context?.displayName
        println("^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ " +
            "\n===>>> start api test $displayName <<<===")
    }

    override fun afterEach(context: ExtensionContext?) {
        val displayName = context?.displayName

        try {
            val throwable = context?.executionException
            log.error("Test: $displayName , Stop With Error: ${throwable!!.get().message}")
        } catch (nExc: NoSuchElementException) {
            log.warn("Test: $displayName , Stop Without Error")
        }

        println("===>>> stop api test $displayName <<<===" +
            "\n^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ")
    }

    override fun afterAll(context: ExtensionContext?) {
        println("^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ " +
                "\n===>>> STOP TEST ${context!!.testClass?.get()} <<<===")
    }
}