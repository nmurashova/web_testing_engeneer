package ru.sibur.test.engineering.tests.web.functionalTests

import io.qameta.allure.*
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import ru.sibur.test.engineering.tests.base.WebTestBase
import ru.sibur.test.engineering.tests.steps.web.BaseWebSteps
import ru.sibur.test.engineering.tests.steps.web.MainPageSteps

@Owner("MurashovaNN")
@Tags(value = [Tag("web"), Tag("dev")])
class ExampleWebTest : WebTestBase() {

    @Autowired
    private lateinit var baseWebSteps: BaseWebSteps
    @Autowired
    private lateinit var mainPageSteps: MainPageSteps

    @Test
    @AllureId("31778")
    @DisplayName("Example web test")
    fun origin_exampleWebTest() {

        // Предусловия
        baseWebSteps.open()

        mainPageSteps.search("Санкт-Петербург")

        // natalia
        // ввести в поле поиска значение "Санкт-петербург"
        // убедиться, что система вывела погоды именно для санкт-петербурга
        // перейти на вкладку Сегодня, убедиться что система отобразила нужную информацию/

    }

    @Test
    @DisplayName("Example web test Arko")
    @AllureId("31790")
    fun exampleIliukhinTest() {

        // Предусловия
        baseWebSteps.open()

        mainPageSteps.searchWeatherForWeek("Неделя")

        val result = mainPageSteps.getStringResult()

        Assertions.assertTrue(result.contains("на неделю"))

        // Аркадий
        // выбрать в выпадающем списке "Ещё" прогноз на неделю
        // Прогноз погоды отобразился на неделю

    }

    @Test
    @DisplayName("Example web test")
    @AllureId("31789")
    fun zavolokinatp() {

        // Предусловия
        baseWebSteps.open()

        mainPageSteps.selectPopular("Барнаул")

        val str1 = mainPageSteps.getHeader()

        Assertions.assertEquals("Погода в Барнауле сегодня", str1)

        mainPageSteps.selectSubnavMenu("10 дней")

        val str2 = mainPageSteps.getHeader()

        Assertions.assertEquals("Погода в Барнауле на 10 дней", str2)

        //тест2
        //в "Популярные пункты России" выбрать Барнаул
        //убедиться, что система вывела погоду именно для Барнаула
        //нажать на кнопку "10 дней"
        //убедиться, что отображается погода для Барнаула на 10 дней

    }

}