package ru.sibur.test.engineering.tests.extensions

import io.qameta.allure.Step
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.extension.*
import org.slf4j.LoggerFactory
import org.springframework.test.context.junit.jupiter.SpringExtension
import ru.sibur.test.engineering.model.AliasConsts.Companion.IS_MANUAL
import ru.sibur.test.engineering.model.AliasConsts.Companion.WEB_ALIAS
import ru.sibur.test.engineering.properties.CommonProperties
import ru.sibur.test.engineering.utils.BaseUtility
import ru.sibur.test.engineering.tests.context.IntegrationTestContext
import ru.sibur.test.engineering.SeleniumFactory
import ru.sibur.test.engineering.model.WebTestContext
import ru.sibur.test.engineering.properties.WebProperties
import ru.sibur.test.engineering.utils.BaseWebUtility

class WebTestExtension :
    BeforeAllCallback,
    BeforeEachCallback,
    AfterEachCallback,
    AfterAllCallback {
    private val log = LoggerFactory.getLogger(javaClass)

    private lateinit var integrationTestContext: IntegrationTestContext

    private lateinit var commonProperties: CommonProperties
    private lateinit var baseUtility: BaseUtility
    private lateinit var webProperties: WebProperties
    private lateinit var seleniumFactory: SeleniumFactory

    private var beforeClassInitialization = false
    private var isManualTest = false

    override fun beforeAll(context: ExtensionContext?) {
        println(
            "^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^" +
                    "\n===>>> START TEST ${context!!.testClass?.get()} <<<==="
        )

        integrationTestContext = SpringExtension.getApplicationContext(context)
            .getBean(IntegrationTestContext::class.java)
        commonProperties = SpringExtension.getApplicationContext(context)
            .getBean(CommonProperties::class.java)
        webProperties = SpringExtension.getApplicationContext(context)
            .getBean(WebProperties::class.java)
        seleniumFactory = SpringExtension.getApplicationContext(context)
            .getBean(SeleniumFactory::class.java)
        baseUtility = SpringExtension.getApplicationContext(context)
            .getBean(BaseUtility::class.java)

        checkBeforeInitializationAnnotation(context)

        if (beforeClassInitialization) {
            initializationWebdriver()
        }

        println(
            "===>>> STOP WEB BEFORE ALL CALLBACK ${context.testClass?.get()} <<<===" +
                    "\n^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ "
        )
    }

    private fun checkBeforeInitializationAnnotation(context: ExtensionContext?) {
        beforeClassInitialization = context!!.testClass!!.get()
            .annotations.toMutableList().filter {
                it.annotationClass.simpleName == "BeforeClassInitialization"
            }.any()
    }

    private fun initializationWebdriver() {
        val webTestContext = WebTestContext()
        webTestContext.webProperties = webProperties
        webTestContext.baseUtility = baseUtility

        val wd = seleniumFactory.selectLocalOrServer(webTestContext)
        webTestContext.wd = wd
        val softly = SoftAssertions()
        webTestContext.softly = softly
        val baseWebUtility = BaseWebUtility.base(webTestContext)
        integrationTestContext.storeUI(WEB_ALIAS, baseWebUtility)
    }

    override fun beforeEach(context: ExtensionContext?) {
        val isManual = context!!.testMethod.get().annotations.toMutableList()
            .filter { it.annotationClass.simpleName.equals("Manual") }.any()
        integrationTestContext.store(IS_MANUAL, isManual)
        if (!beforeClassInitialization && !isManual) {
            println(
                "^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ " +
                        "\n===>>> START WEB BEFORE EACH CALLBACK ${context!!.displayName} <<<==="
            )

            initializationWebdriver()

            println(
                "===>>> STOP WEB BEFORE EACH CALLBACK ${context.displayName} <<<===" +
                        "\n^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ "
            )
        }
    }

    override fun afterEach(context: ExtensionContext?) {
        val displayName = context!!.displayName
        println("^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ " +
                "\n===>>> START WEB AFTER EACH CALLBACK $displayName <<<===")

        try {
            val throwable = context.executionException
            if (throwable!!.get().message!!.length > 1000)
                log.error("test: $displayName , stop with error: ${throwable.get().message!!.substring(0, 1000)}")
            else
                log.error("test: $displayName , stop with error: ${throwable.get().message!!}")
            savedScreenshot(context, displayName)
        } catch (nExc: NoSuchElementException) {
            log.warn("test: $displayName , stop without error")
//            context?.let { savedScreenshot(it, displayName) }
        }


        if (!beforeClassInitialization && !integrationTestContext.fetch(IS_MANUAL, Boolean::class)) {
            try {
                softlyAssertAll()
            } finally {
                quitWebdriver()
            }
        }

        println("===>>> STOP WEB AFTER EACH CALLBACK $displayName <<<===" +
                "\n^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ")
    }

    override fun afterAll(context: ExtensionContext?) {
        println(
            "^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ " +
                    "\n===>>> START WEB AFTER ALL CALLBACK ${context!!.testClass?.get()} <<<==="
        )

        if (beforeClassInitialization) {
            try {
                softlyAssertAll()
            } finally {
                quitWebdriver()
            }
        }

        println(
            "===>>> STOP TEST  ${context.testClass?.get()} <<<===" +
                    "\n^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ "
        )
    }

    @Step("Save Screenshot")
    private fun savedScreenshot(context: ExtensionContext, displayName: String) {
        val baseWebUtility = integrationTestContext.fetchUi(WEB_ALIAS)
        baseWebUtility.takeScreenshot("${context.testClass?.get()!!}_${displayName}")
    }


    private fun quitWebdriver() {
        integrationTestContext.fetchUi(WEB_ALIAS).quitWd()
        integrationTestContext.clearUi(WEB_ALIAS)
    }

    private fun softlyAssertAll() {
        integrationTestContext.fetchUi(WEB_ALIAS).assertAll()
    }
}