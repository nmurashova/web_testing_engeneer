package ru.sibur.test.engineering.tests.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("ru.sibur.test.svt")
class TestConfig