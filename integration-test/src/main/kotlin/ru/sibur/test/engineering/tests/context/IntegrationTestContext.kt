package ru.sibur.test.engineering.tests.context

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import ru.sibur.test.engineering.utils.BaseWebUtility
import java.util.*
import kotlin.reflect.KClass

@Component
class IntegrationTestContext {
    private val log = LoggerFactory.getLogger(javaClass)

    private var objectContext: MutableMap<String, Any> = HashMap()

    fun <T: Any> fetch(alias: String, clazz: KClass<T>): T {
        val classLocalName = classLocalName(clazz, alias)
        return objectContext[classLocalName] as T
    }

    fun store(alias: String, item: Any) {
        val classLocalName = classLocalName(item::class, alias)
        log.info("store object: $item with alias: $classLocalName")
        objectContext[classLocalName] = item
    }

    /**
     * получает ключ, уникальный в рамках одного теста и конвертирует в глобально-уникальный с помощью
     * threadLocalName().
     */
    fun fetchUi(alias: String) = objectContext[threadLocalName(alias)] as BaseWebUtility

    fun storeUI(alias: String, webUtility: BaseWebUtility) {
        val threadLocalName = threadLocalName(alias)
        log.info("store ui manager object with alias $threadLocalName")
        objectContext[threadLocalName] = webUtility
    }

    fun clearUi(alias: String) {
        val threadLocalName = threadLocalName(alias)
        log.info("clear ui manager object with alias $threadLocalName")
        objectContext.remove(threadLocalName) as BaseWebUtility
    }

    companion object {
        fun <T: Any> classLocalName(clazz: KClass<T>, alias: String): String {
            return "${clazz.simpleName!!.lowercase(Locale.getDefault())}-$alias"
        }

        /**
         * Конвертирует уникальный для теста {@code alias} в глобально уникальную строку, добавляя идентификатор
         * текущего потока.
         *
         * @param alias уникальный для теста идентификатор, который необходимо преобразовать в глобально уникальный
         * идентификатор.
         */
        fun threadLocalName(alias: String): String {
            val t = Thread.currentThread()
            return "$alias-${t.id}${t.name}"
        }
    }
}