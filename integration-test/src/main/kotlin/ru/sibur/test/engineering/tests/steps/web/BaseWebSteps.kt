package ru.sibur.test.engineering.tests.steps.web

import io.qameta.allure.Step
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.sibur.test.engineering.model.AliasConsts.Companion.WEB_ALIAS
import ru.sibur.test.engineering.properties.CommonProperties
import ru.sibur.test.engineering.tests.context.IntegrationTestContext

@Component
class BaseWebSteps {

    @Autowired
    private lateinit var integrationTestContext: IntegrationTestContext
    @Autowired
    private lateinit var commonProperties: CommonProperties

    /**
     * получаем объект UI менеджер из контекста
     * по alias по умолчанию "webdriver"
     */
    fun ui(alias: String = WEB_ALIAS) = integrationTestContext.fetchUi(alias)

    @Step("Open application Url")
    fun open(url: String = commonProperties.web.baseUrl) {
        ui().open(url)
    }
}