package ru.sibur.test.engineering.tests.steps.web

import io.qameta.allure.Step
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.sibur.test.engineering.model.AliasConsts.Companion.WEB_ALIAS
import ru.sibur.test.engineering.properties.CommonProperties
import ru.sibur.test.engineering.tests.context.IntegrationTestContext

@Component
class MainPageSteps {
    private val log = LoggerFactory.getLogger(javaClass)

    @Autowired
    private lateinit var commonProperties: CommonProperties
    @Autowired
    private lateinit var baseWebSteps: BaseWebSteps

    @Step("Поиск города \"{city}\"")
    fun search(city: String) {
        with (baseWebSteps.ui().mainPageUtil()) {
            searchFieldFill(city) // заполнение поле
            selectCity(city) // выбор города
        }
    }

    @Step ("Прогноз погоды на неделю")
    fun searchWeatherForWeek (value: String) {
        with(baseWebSteps.ui().mainPageUtil()) {
            searchDropDownMenu() // поиск выпадающего списка
            selectValue(value)

            checkResult() // проверка, что определился прогноз погоды на неделю
        }
    }

    fun getStringResult () : String {
        with(baseWebSteps.ui().mainPageUtil()) {
           return getValueResult()
        }
    }

    @Step("Выбор популярного города \"{city}\"")
    fun selectPopular(city: String) {
        with (baseWebSteps.ui().mainPageUtil()) {
            selectPopularCity(city) // выбор города
        }
    }

    @Step("Получение заголовка страницы")
    fun getHeader(): String {
        with (baseWebSteps.ui().mainPageUtil()) {
            val str = getHeaderOnPopularPage()
            return str
        }
    }
    @Step("Выбор элемента в сабменю \"{item}\"")
    fun selectSubnavMenu(item: String) {
        with (baseWebSteps.ui().mainPageUtil()) {
            selectSubnavMenuItem(item) // выбор элемента в сабменю
        }
    }


}