package ru.sibur.test.engineering.tests

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class IntegrationApplication

fun main(args: Array<String>) {
    SpringApplication.run(IntegrationApplication::class.java, *args)
}