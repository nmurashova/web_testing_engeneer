package ru.sibur.test.engineering.tests.context

data class ApiTestContext (
    var integrationTestContext: IntegrationTestContext? = null
)