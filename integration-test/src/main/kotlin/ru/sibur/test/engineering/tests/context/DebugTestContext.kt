package ru.sibur.test.engineering.tests.context

import ru.sibur.test.engineering.properties.CommonProperties
import ru.sibur.test.engineering.utils.BaseUtility

data class DebugTestContext(
    var integrationTestContext: IntegrationTestContext? = null,
    var commonProperties: CommonProperties? = null,
    var baseUtility: BaseUtility? = null
)