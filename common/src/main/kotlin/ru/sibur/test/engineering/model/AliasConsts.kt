package ru.sibur.test.engineering.model

class AliasConsts {

    companion object {
        const val WEB_ALIAS = "web-alias"
        const val WEB_ALIAS_SECOND = "web-alias-second"
        const val DEFAULT_ROLE = "analyst"
        const val IS_MANUAL = "is_manual"
//        const val SECOND_ROLE = "manager"

        /**
         * API
         */
        const val AUTH = "auth" //login


        // users
        const val USER_ID = "id"
        const val USER_LOGIN = "login"
        const val USER_PASSWORD = "password"
        const val USER_FIRSTNAME = "firstname"
        const val USER_LASTNAME = "lastname"
        const val USER_PATRONYMICNAME = "patronymicname"
        const val USER_LIKE_LASTNAME = "like_lastname"
        const val USER_ROLE_ID = "role_id"
        const val USER_POSITION_ID = "position_id"
        const val USER_ENTERPRISE_ID = "enterprise_id"
        const val USER_PRODUCTION_ID = "production_id"
        const val USER_FACILITY_ID = "facility_id"

        const val USER_IS_RESPONSIBLE_MANAGER = "is_responsible_manager"
        const val USER_ACTIVE = "active"

        // common
        const val SORT = "sort"
        const val ORDER = "order"
        const val LIMIT = "limit"
        const val OFFSET = "offset"

        // enterprise
        const val ENTERPRISE_ID_SHAREPOINT = "shr_id"
        const val ENTERPRISE_NAME = "name"
        const val ENTERPRISE_LIKE_NAME = "like_name"
        const val ENTERPRISE_MANAGER = "manager"
        const val ENTERPRISE_MANAGER_ID = "manager_id"
        const val ENTERPRISE_MARKER_TYPE = "type_marker"
        const val ENTERPRISE_ACTIVE = "active"

        const val PLACE_OF_WORK_POSITION_ID = ""
        const val PLACE_OF_WORK_ENTERPRISE_ID = ""

        // production
        const val PRODUCTION_ID = "id"
        const val PRODUCTION_ID_SHAREPOINT = "shr_jrnl_id"
        const val PRODUCTION_NAME = "name"
        const val PRODUCTION_LIKE_NAME = "like_name"
        const val PRODUCTION_ENTERPRISE_ID = "enterprise_id"
        const val PRODUCTION_ENTERPRISE_NAME = "enterprise_name"
        const val PRODUCTION_RESPONSIBLE_ID = "responsible_person_id"
        const val PRODUCTION_RESPONSIBLE_NAME = "responsible_person_name"
        const val PRODUCTION_ACTIVE = "active"

        // facility
        const val FACILITY_ID_SHAREPOINT = "shr_jrnl_id"
        const val FACILITY_NAME = "name"
        const val FACILITY_LIKE_NAME = "like_name"
        const val FACILITY_ENTERPRISE_ID = "enterprise_id"
        const val FACILITY_ENTERPRISE_NAME = "enterprise_name"
        const val FACILITY_PRODUCTION_ID = "production_id"
        const val FACILITY_PRODUCTION_NAME = "production_name"
        const val FACILITY_ACTIVE = "active"


        // position
        const val POSITION_OWNER_ENTERPRISE_ID = "owner_enterprise_id"
        const val POSITION_LIKE_NAME = "like_name"
        const val POSITION_ACTIVE = "active"
        const val POSITION_NAME = "name"
        const val POSITION_ID = "id"
        const val POSITION_CHECK_MARKER = "check_marker"
        const val POSITION_COEFF_GUEST_AUDIT = "coeff_guest_audit"

        // npa
        const val NPA_ID = "id"
        const val NPA_TEXT = "text"
        const val NPA_NAME = "text"
        const val NPA_LIKE_TEXT = "like_text"
        const val NPA_ACTIVE = "active"

        // questions
        const val QUESTION_OWNER_ENTERPRISE_ID = "owner_enterprise_id"
        const val QUESTION_ID = "id"
        const val QUESTION_ID_FROM = "id_from"
        const val QUESTION_ID_TO = "id_to"
        const val QUESTION_DIRECT_OF_MARK_ID = "direct_of_mark_id" // course
        const val QUESTION_COURSE = "course"
        const val QUESTION_AREA_OF_MARK_ID = "area_of_mark_id"
        const val QUESTION_AREA = "area"
        const val QUESTION_TARGET_OF_MARK_ID = "target_of_mark_id"
        const val QUESTION_ITEM = "item"
        const val QUESTION_SEASON_ID = "season_id"
        const val QUESTION_SEASON = "season"
        const val QUESTION_TEXT = "question_text"
        const val QUESTION_INSTRUCTIONS_TEXT = "instructions_text"
        const val QUESTION_NPA = "npa"
        const val QUESTION_NPA_ID = "npa_id"
        const val QUESTION_POSITION_ID = "position_id"
        const val QUESTION_POSITION = "position"
        const val QUESTION_BINDING_POSITION = "binding_position"
        const val QUESTION_UNBINDING_POSITION = "unbinding_position"
        const val QUESTION_COEFF = "coeff"
        const val QUESTION_OLD_QUESTION_ID = "old_question_id"
        const val QUESTION_OLD_QUESTION_ID_FROM = "old_id_from"
        const val QUESTION_OLD_QUESTION_ID_TO = "old_id_to"
        const val QUESTION_ACTIVE = "active"
        const val QUESTION_QUESTION_LIBRARY_ID = "question_library_id"
        const val QUESTION_LIKE_TEXT = "like_text"

        // questionsSubDivision
        const val QUESTION_SUBDIV_ID = "id"
        const val QUESTION_SUBDIV_ID_FROM = "id_from"
        const val QUESTION_SUBDIV_ID_TO = "id_to"
        const val QUESTION_SUBDIV_CREATION_DATE_FROM = "created_time_from"
        const val QUESTION_SUBDIV_CREATION_DATE_TO = "created_time_to"
        const val QUESTION_SUBDIV_COURSE = "course"
        const val QUESTION_SUBDIV_AREA = "area"
        const val QUESTION_SUBDIV_ITEM = "item"
        const val QUESTION_SUBDIV_DIRECT_OF_MARK_ID = "direct_of_mark_id" // course
        const val QUESTION_SUBDIV_AREA_OF_MARK_ID = "area_of_mark_id"
        const val QUESTION_SUBDIV_TARGET_OF_MARK_ID = "target_of_mark_id"
        const val QUESTION_SUBDIV_QUESTION_ID = "question_id"
        const val QUESTION_SUBDIV_QUESTION = "question"
        const val QUESTION_SUBDIV_QUESTION_ID_FROM = "question_id_from"
        const val QUESTION_SUBDIV_QUESTION_ID_TO = "question_id_to"
        const val QUESTION_SUBDIV_FACILITY = "facility"
        const val QUESTION_SUBDIV_FACILITY_ID = "facility_id"
        const val QUESTION_SUBDIV_PRODUCTION = "production"
        const val QUESTION_SUBDIV_PRODUCTION_ID = "production_id"
        const val QUESTION_SUBDIV_ENTERPRISE = "enterprise"
        const val QUESTION_SUBDIV_ENTERPRISE_ID = "enterprise_id"
        const val QUESTION_SUBDIV_LIKE_TEXT = "like_text"

        // question Library
        const val QUESTION_LIBRARY_ENTERPRISE = "enterprise_name"
        const val QUESTION_LIBRARY_COEFF = "coefficient"
        const val QUESTION_LIBRARY_OLD_QUESTION_ID_TO = "old_id_to"
        const val QUESTION_LIBRARY_OLD_QUESTION_ID_FROM = "old_id_from"
        const val QUESTION_LIBRARY_ID_TO = "id_to"
        const val QUESTION_LIBRARY_ID_FROM = "id_from"
        const val QUESTION_LIBRARY_LIKE_TEXT = "like_text"
        const val QUESTION_LIBRARY_ACTIVE = "active"
        const val QUESTION_LIBRARY_OLD_QUESTION_ID = "old_question_id"
        const val QUESTION_LIBRARY_POSITION = "position"
        const val QUESTION_LIBRARY_POSITION_ID = "position_id"
        const val QUESTION_LIBRARY_NPA = "npa"
        const val QUESTION_LIBRARY_NPA_ID = "npa_id"
        const val QUESTION_LIBRARY_INSTRUCTIONS_TEXT = "instructions_text"
        const val QUESTION_LIBRARY_TEXT = "question_text"
        const val QUESTION_LIBRARY_SEASON = "season"
        const val QUESTION_LIBRARY_SEASON_ID = "season_id"
        const val QUESTION_LIBRARY_ITEM = "item"
        const val QUESTION_LIBRARY_TARGET_OF_MARK_ID = "target_of_mark_id"
        const val QUESTION_LIBRARY_AREA = "area"
        const val QUESTION_LIBRARY_AREA_OF_MARK_ID = "area_of_mark_id"
        const val QUESTION_LIBRARY_COURSE = "course"
        const val QUESTION_LIBRARY_DIRECT_OF_MARK_ID = "direct_of_mark_id"
        const val QUESTION_LIBRARY_ID = "id"

        // filter
        const val FILTER_STATUS = "status"
        const val FILTER_ID_FROM = "id_from"
        const val FILTER_ID_TO = "id_to"
        const val FILTER_QUESTION_ID_FROM = "question_id_from"
        const val FILTER_QUESTION_ID_TO = "question_id_to"
        const val FILTER_OLD_ID_FROM = "old_id_from"
        const val FILTER_OLD_ID_TO = "old_id_to"
        const val FILTER_ID_MIN = "idMin"
        const val FILTER_ID_MAX = "idMax"
        const val FILTER_TASK_TYPE = "taskType"
        const val FILTER_COURSE = "course"
        const val FILTER_AREA = "area"
        const val FILTER_ITEM = "item"
        const val FILTER_SEASON = "season"
        const val FILTER_CREATION_DATE_FROM = "creation_date_from"
        const val FILTER_CREATION_DATE_TO = "creation_date_to"
        const val FILTER_FINISH_DATE_MIN = "finishDateMin"
        const val FILTER_FINISH_DATE_MAX = "finishDateMax"
        const val FILTER_ANSWER_DATE_MIN = "answerDateMin"
        const val FILTER_ANSWER_DATE_MAX = "answerDateMax"
        const val FILTER_NPA = "npa"
        const val FILTER_ENTERPRISE = "enterprise"
        const val FILTER_EXECUTOR_POSITION = "executorPosition"
        const val FILTER_EXECUTOR = "executor"
        const val FILTER_PRODUCTION = "production"
        const val FILTER_POSITION = "position"
        const val FILTER_FACILITY = "facility"
        const val FILTER_RESULT = "result"
        const val FILTER_ACTIVE = "active"


    }
}