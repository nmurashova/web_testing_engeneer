package ru.sibur.test.engineering.utils

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.qameta.allure.Attachment
import io.qameta.allure.Step
import org.apache.commons.csv.CSVFormat
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import ru.sibur.test.engineering.model.TestConsts
import java.io.*
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.function.Consumer
import java.util.regex.Matcher
import kotlin.io.path.Path

@Component
class BaseUtility {
    private val log = LoggerFactory.getLogger(javaClass)

    fun <T> getModelFromJson(path: String?, objectClass: Class<T>?): T {
        return try {
            val jsonFile = this::class.java.getResourceAsStream(path)!!
            val objectMapper = ObjectMapper()
            return objectMapper.readValue(jsonFile, objectClass) as T
        } catch (e: IOException) {
            e.printStackTrace()
            throw RuntimeException()
        }
    }

    fun readCsvFile(pathToFile: String): List<List<String>>? {
        val result: MutableList<List<String>> = LinkedList()
        val resource = this::class.java.getResource(pathToFile).file

        try {
            Files.newBufferedReader(Path(resource), StandardCharsets.UTF_8).use { reader ->
                CSVFormat.RFC4180.withFirstRecordAsHeader().parse(reader).use { parser ->
                    for (record in parser) {
                        val row: MutableList<String> = LinkedList()
                        record.forEach(Consumer { e: String -> row.add(e) })
                        result.add(row)
                    }
                }
            }
        } catch (e: IOException) {
            throw UncheckedIOException(e)
        }

        return result
    }

    fun readCsvFileFromDownloads(pathToFile: String): List<List<String>> {
        val result: MutableList<List<String>> = LinkedList()
        val resource = pathToFile
        val srcCharset = Charset.forName("windows-1251")

        try {
            Files.newBufferedReader(Path(resource), srcCharset).use { reader ->
                CSVFormat.RFC4180.withFirstRecordAsHeader().withDelimiter(';').parse(reader).use {
                    parser ->
                    for (record in parser) {
                        val row: MutableList<String> = LinkedList()
                        record.forEach(Consumer { e: String -> row.add(e) })
                        result.add(row)
                    }
                }
            }
        } catch (e: IOException) {
            throw UncheckedIOException(e)
        }

        return result
    }

    fun getTomorrowDate() : String {
        val dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
        val tomorrow = LocalDate.now().plusDays(1)
        return tomorrow.format(dateFormatter)
    }

    fun <T> deserializer(pathToFile: String,
                         clazz: Class<T>,
                         jsonString: String): T? {
        val mapper = ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true)
        try {
            return mapper.readValue(jsonString, clazz)
        } catch (ex: IOException) {
            log.error(String.format("Not deserialization object in file: %s, error: %s", pathToFile, ex))
            return null
        }
    }

    fun replaceWithoutSpace(reception: String, logger: Boolean): String {
        val replace = reception.replace(" ", "_")
        if (logger) {
            log.warn("reception str: $reception , replace str: $replace")
        }

        val replace2 = replace.replace("[\\W]".toRegex(), "_")
        if (logger) {
            log.warn("reception str: $replace , replace str: $replace2")
        }

        return replace2
    }

    @Step("{logger}")
    fun allureLogger(logger: String, isError: Boolean) {
        if (isError) {
            log.error(logger)
        } else {
            log.warn(logger)
        }
    }

    @Step("Get current date")
    fun getCurrentDateStr(format: String): String {
        val dateFormat = SimpleDateFormat(format)
        return dateFormat.format(Date())
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    fun attacheScreenshot(filename: String): ByteArray? {
        var fileContent: ByteArray? = null
        try {
            fileContent =
                    Files.readAllBytes(
                            Paths.get(filename))

//                    fileContent = javaClass.classLoader.getResourceAsStream(filename).readAllBytes()!!

        } catch (e: IOException) {
            e.printStackTrace()
        }

        log.warn("attachment screenshot for allure $filename")
        return fileContent
    }

    fun getFileNameForAttachedScreenshot(): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS")
        val currentDateTime = dateFormat.format(Date())

        return "$currentDateTime.png"
    }

    @Step("забираем матчер по регулярному выражению {regex} из {text}")
    fun getRegexMatcher(regex: String, text: String): Matcher {
        log.warn("search to regex $regex in text: $text")
        val pattern = regex.toRegex().toPattern()
        return pattern.matcher(text)
    }

    @Step("забираем строку по регулярному выражению {regex} из {text}")
    fun regexp(regex: String, text: String): String? {
        val matcher = getRegexMatcher(regex, text)
        if (matcher.find()) {
            return matcher.group().toString()
        }
        log.error("target text not found")
        return null
    }

    /**
     * сериализуем объект и записываем в файл
     */
    fun <T> writeObject(pathToFile: String, obj: T) {
        try {
            val serialized = gsonSerialazer(obj)
//            val serialized = jacksonSerialazer(obj)
            writeFile(pathToFile, serialized)
        } catch (exc: Exception) {
            log.error("not write object: $obj, error: $exc")
        }
    }

    fun jacksonMapper(): ObjectMapper {
//        val mapper = jacksonObjectMapper()
        val mapper = ObjectMapper().registerModule(KotlinModule())
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true)
        return mapper
    }

    fun gsonBuilder(): Gson {
        return GsonBuilder()
            .setPrettyPrinting()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            .serializeNulls()
            .create()
    }

    private fun <T> jacksonSerialazer(obj: T): String {
        val mapper = jacksonMapper()
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true)
        return mapper.writeValueAsString(obj)
    }

    private fun <T> gsonSerialazer(obj: T): String {
        val type = object: TypeToken<T>() {}.type
        val serializad = gsonBuilder().toJson(obj, type)
        return serializad
    }

    fun <T> jacksonDeserialazer(jsonString: String, clazz: Class<T>): T {
        val mapper = jacksonMapper()
        return mapper.readValue(
                jsonString,
                object: TypeReference<T>() {})
    }

    fun <T> gsonDeserialazer(jsonString: String, clazz: Class<T>): T {
        val type = object: TypeToken<T>() {}.type
        return gsonBuilder().fromJson(jsonString, type)
    }

    /**
     *  запись в файл текст
     */
    fun writeFile(path: String, content: String) {
        try {
            FileWriter(File(path)).use { writer -> writer.write(content) }
        } catch (exc: Exception) {
            log.error("not write file: $content to file: $path, Error: $exc")
        }
    }

    /**
     *  запись в файл байт
     */
    fun writeFile(path: String, map: MutableMap<String, Any>) {
        try {
            val fileOut = FileOutputStream(File(path))
            val out = ObjectOutputStream(fileOut as OutputStream?)
            out.writeObject(map)
            out.close()
            fileOut.close()
        } catch (exc: Exception) {
            exc.printStackTrace()
            log.error("not write file: $map to file: $path, error: $exc")
        }
    }

    /**
     *  чтение из файла байт
     *  кастим к MutableMap<String, Any>
     */
    fun readTestContextFile(path: String): MutableMap<String, Any>? {
        try {
            val fileIn = FileInputStream(path)
            val oin = ObjectInputStream(fileIn)
            val e = oin.readObject() as MutableMap<String, Any>
            oin.close()
            fileIn.close()
            return e
        } catch (exc: Exception) {
            log.error("not reading file: $path, error: $exc")
            return null
        }
    }

    /**
     *  читаем файл
     */
    fun readFile(pathToFile: String): String? {
        try {
            BufferedReader(FileReader(pathToFile)).use { br ->
                var json = ""
                var line: String? = br.readLine()
                while (line != null) {
                    json += line
                    line = br.readLine()
                }
                return json
            }
        } catch (e: IOException) {
            log.error("not reading file: $pathToFile")
            return null
        }
    }

    /**
     * объявляется критичность добавляемых в ассерты проверок
     * low ->     добавляются результаты всех проверок
     * medium ->  добавляются проверки помеченные
     *            medium и high
     * high ->    добавляются проверки помеченные high
     */
    @Value("\${add_assert:low}")
    lateinit var ADD_ASSERT: String
    /**
     * объявляется критичность проверок на которых вызывать исключение
     * low ->  вызывается на всех проверках
     * medium ->  вызывается на проверках помеченных
     *            medium и high
     * high ->  вызывается на проверках помеченных high
     */
    @Value("\${assert_throws:high}")
    lateinit var ASSERT_THROWS: String

    /**
     * обработка результата проверки
     * @param testResult => результат проверки
     * @param criticality => критичность проверяемой фичи
     * @param positiveLogger => положительный текст проверки для allure
     * @param failedLogger => отрицательный текст проверки для allure
     *
     * объявлем списки критичности проверок
     *
     * выбираем какой текст лога добавлять
     *
     * если уровень критичности для добавления в ассерты
     * ниже заявленного всегда возвращается true в allure пишется текст проверки
     * , а если нет то возвращается актуальный результат с отчетом в allure
     *
     * если уровень критичности для вызова исключения соответствует или выше заявленного
     * , а так же если результат проверки отрицательный вызывается исключение
     */
    fun handlerAssertResult(testResult: Boolean,
                            criticality: String,
                            positiveLogger: String,
                            failedLogger: String?): Boolean {
        val three =
            listOf(TestConsts.LOW, TestConsts.MEDIUM, TestConsts.HIGH)
        val two =
            listOf(TestConsts.MEDIUM, TestConsts.HIGH)
        val one =
            listOf(TestConsts.HIGH)

        val enableAssert: Boolean =
            validationCriticality(three, two, one, criticality, ADD_ASSERT)

        val enableThrows: Boolean =
            validationCriticality(three, two, one, criticality, ASSERT_THROWS)

        val logger: String?
        if (testResult) {
            logger = positiveLogger
        } else {
            logger = failedLogger
        }

        if (enableAssert) {
            if (logger != null) { allureLogger(logger, !testResult) }
            if (enableThrows) {
                if (!testResult) {
                    if (logger != null)
                        throw AssertionError()
                    else
                        throw AssertionError("блокирующая ошибка: $logger")
                }
            }
            return testResult
        } else {
            allureLogger("проверки уровня критичности $criticality отключены, " +
                    "сообщение проверки: $logger", false)
            return true
        }
    }

    /**
     * @param currentCriticality => критичность текущей проверки
     * @param targetCriticality => минимальный уровень критичности проверок
     * проверяем входит ли текущий уровень критичности
     * в список заявленных
     */
    private fun validationCriticality(
            three: List<String>,
            two: List<String>,
            one: List<String>,
            currentCriticality: String,
            targetCriticality: String): Boolean {
        return when (targetCriticality) {
            TestConsts.LOW -> three.contains(currentCriticality)
            TestConsts.MEDIUM -> two.contains(currentCriticality)
            TestConsts.HIGH -> one.contains(currentCriticality)
            else -> false
        }
    }

    /**
     *
     */
    fun mutliMatcher(vararg asserts: () -> Unit): Boolean {
        return asserts.map {
            try {
                it()
                return@map true
            } catch (exc: AssertionError) {
                allureLogger(exc.message!!, true)
                return@map false
            }
        }.all {
            it.equals(true)
        }
    }

}