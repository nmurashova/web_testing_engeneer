package ru.sibur.test.engineering.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "test", ignoreInvalidFields = true)
class CommonProperties {

    val web = Web()
    val api = Api()
    val db = Db()

    class Web {
        lateinit var baseUrl: String
        lateinit var username: String
        lateinit var password: String
    }

    class Api {
        lateinit var username: String
        lateinit var password: String
        lateinit var baseUrl: String
        lateinit var authUrl: String
    }

    class Db {
        lateinit var host: String
        lateinit var port: String
        lateinit var name: String
        lateinit var username: String
        lateinit var password: String
    }
}