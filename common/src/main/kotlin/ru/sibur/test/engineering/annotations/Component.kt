package ru.sibur.test.engineering.annotations

import io.qameta.allure.LabelAnnotation
import io.qameta.allure.Stories
import io.qameta.allure.util.ResultsUtils
import java.lang.annotation.*
import java.lang.annotation.Repeatable
import java.lang.annotation.Retention

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(
        AnnotationTarget.FUNCTION,
        AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER,
        AnnotationTarget.ANNOTATION_CLASS,
        AnnotationTarget.CLASS)
@LabelAnnotation(name = "Component")
annotation class Component(val value: String)
