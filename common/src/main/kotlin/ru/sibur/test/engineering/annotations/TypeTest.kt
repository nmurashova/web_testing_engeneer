package ru.sibur.test.engineering.annotations

import io.qameta.allure.LabelAnnotation
import java.lang.annotation.*
import java.lang.annotation.Retention
import java.lang.annotation.Target

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@kotlin.annotation.Target(
        AnnotationTarget.FUNCTION,
        AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER,
        AnnotationTarget.ANNOTATION_CLASS,
        AnnotationTarget.CLASS)
@LabelAnnotation(name = "type")
annotation class TypeTest(val value: String)
