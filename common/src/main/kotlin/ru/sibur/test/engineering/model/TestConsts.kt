package ru.sibur.test.engineering.model

class TestConsts {
    companion object {
        val TRUE = "true"
        val FALSE = "false"
        val HIGH = "high"
        val MEDIUM = "medium"
        val LOW = "low"
    }
}